﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text;
using System.IO;

namespace Snakes___The_Evolution_CLI_Client
{
    class GameFiles
    {
        public class GamePlayerData
        {
            private string Path;

            public GameFilesTypes.GanePlayerData PlayerData { get; set; }

            public void UpdateCache()
            {
                PlayerData = JsonSerializer.Deserialize<GameFilesTypes.GanePlayerData>(File.ReadAllText(Path));
            }

            public void Save()
            {

                File.WriteAllText(Path, JsonSerializer.Serialize(PlayerData));
            }

            public GamePlayerData (string path)
            {
                Path = path;
                try
                {
                    
                    PlayerData = JsonSerializer.Deserialize<GameFilesTypes.GanePlayerData>(File.ReadAllText(Path));
                }
                catch (Exception)
                {

                    PlayerData = new GameFilesTypes.GanePlayerData
                    {
                        Player = $"Player_{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}{new Random().Next(0, 9)}",
                        XP = 0,
                        PreferedForegroundColor = ConsoleColor.Black,
                        PreferedBackgroundColor = ConsoleColor.Blue,
                        PreferedSnakeRepr = '%',
                        Trophies = new List<Types.Trophie_Data>(),
                        Statistics = new Types.Statistics_Data
                        {
                            Games_Scores = new List<int>(),
                            Total_Score = 0,
                            Games_Played = 0,
                            Games_Lost = 0,
                        }

                    };
                    File.WriteAllText(Path, JsonSerializer.Serialize(PlayerData));
                }
            }
        }
    }

    class GameFilesTypes
    {
        public class GanePlayerData
        {
            public string Player { get; set; }

            public int XP { get; set; }

            public ConsoleColor PreferedForegroundColor { get; set; }

            public ConsoleColor PreferedBackgroundColor { get; set; }

            public char PreferedSnakeRepr { get; set; }

            public List<Types.Trophie_Data> Trophies { get; set; }

            public Types.Statistics_Data Statistics { get; set; }

        }

    }
}
